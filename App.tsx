import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";

import useCachedResources from "./hooks/useCachedResources";
import useColorScheme from "./hooks/useColorScheme";
import Navigation from "./navigation";
import axios from "axios";
import { Config } from "./Config";

import { useFonts, Comfortaa_400Regular } from "@expo-google-fonts/comfortaa";

import { Audio, AVPlaybackStatus } from "expo-av";
import { NotifierWrapper } from "react-native-notifier";
import PlayerService from "./service/PlayerService";

function setUpAxios() {
  axios.defaults.baseURL = Config.getBotBApiUrl();
  axios.defaults.headers.post["Content-Type"] = "application/json";
}

export type BotbServiceAnimation = {
  play: () => void;
  stop: () => void;
};

export type RadioContextType = {
  currentSound?: Audio.Sound;
  playerService: PlayerService;
  botbServiceAnimation: BotbServiceAnimation;
  play: (sound: Audio.Sound) => void;
  pause: () => void;
  addSoundListener: (listener: SoundListener) => void;
  setBotbServiceAnimation: (botbService: BotbServiceAnimation) => void;
};

export const RadioContext = React.createContext<RadioContextType>({});

export type SoundListener = {
  update: (status: AVPlaybackStatus) => void;
  newSong?: () => void;
};

const playerService = new PlayerService();

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  const [currentSound, setCurrentSound] = useState({} as Audio.Sound);
  currentSound.setOnPlaybackStatusUpdate;
  const [statusSoundListeners, setStatusSoundListeners] = useState(
    [] as SoundListener[]
  );

  const [botbServiceAnimation, setBotbServiceAnimation] = useState(
    {} as BotbServiceAnimation
  );

  useEffect(() => {
    setUpAxios();
  }, []);

  let [fontsLoaded] = useFonts({
    Comfortaa_400Regular,
  });

  async function play(sound?: Audio.Sound) {
    if (!sound && currentSound) {
      await currentSound.playAsync();
    } else if (sound) {
      await currentSound.stopAsync?.();
      await sound.playAsync();

      statusSoundListeners.forEach((listener) => listener.newSong?.());
      setCurrentSound(sound);
      sound.setOnPlaybackStatusUpdate((status) => {
        playerService.setPlaying(status.isPlaying);
        statusSoundListeners.forEach((listener) => listener.update(status));
      });
    }
    botbServiceAnimation.play();
  }

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <NotifierWrapper>
        <RadioContext.Provider
          value={{
            currentSound,
            playerService,
            botbServiceAnimation,
            setBotbServiceAnimation: (
              botbServiceAnimation: BotbServiceAnimation
            ) => {
              console.log("set up botb", botbServiceAnimation);
              setBotbServiceAnimation(botbServiceAnimation);
            },
            addSoundListener: (listener: SoundListener) => {
              setStatusSoundListeners([...statusSoundListeners, listener]);
            },
            play,
            pause: () => {
              if (currentSound) {
                //TODO handle promise
                currentSound
                  .pauseAsync()
                  .then(() => {})
                  .catch(() => {});
                botbServiceAnimation.stop();
              }
            },
          }}
        >
          <SafeAreaProvider>
            <Navigation colorScheme={colorScheme} />
            <StatusBar />
          </SafeAreaProvider>
        </RadioContext.Provider>
      </NotifierWrapper>
    );
  }
}
