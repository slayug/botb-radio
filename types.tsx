export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Radio: undefined;
  TabTwo: undefined;
};

export type RadioParamList = {
  RadioScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};
