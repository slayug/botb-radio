import axios from 'axios';

export function getEntryList(from: number, to: number) {
    return axios.get(`entry/list/${from}/${to}`)
}