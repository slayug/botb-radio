import { Notifier, NotifierComponents } from "react-native-notifier";

export function error(title: string) {
  Notifier.showNotification({
    title: title,
    description: "Check your internet connection, please",
    Component: NotifierComponents.Alert,
    componentProps: {
      alertType: "error",
    },
  });
}
