
export default class PlayerService {

    private playing: boolean;

    constructor() {
        this.playing = false;
    }

    isPlaying() {
        return this.playing;
    }

    setPlaying(playing: boolean) {
        this.playing = playing;
    }

}