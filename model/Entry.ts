import Author from "./Author";
import BotBr from "./BotBr";

export default interface Entry {
    authors: Author[];
    title: string;
    authors_display: string;
    botbr_id: number;
    battle_id: number;
    datetime: string;
    donloads: number;
    favs: number;
    listen_url: string;
    id: number;
    play_url: string;
    posts: number;
    preview_url: string;
    profile_url: string;
    rank: number;
    score: number;
    votes: number;
    botbr: BotBr;
}