
export default interface BotBr {
    avatar_url: string;
    id: number;
    profile_url: string;
}