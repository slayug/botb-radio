export default interface Author {
    aura: string;
    aura_color: string;
    avatar: string;
    avatar_from_time: string;
    id: string;
    name: string;
    profile_url: string;
}