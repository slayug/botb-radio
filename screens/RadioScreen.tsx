import React, { useEffect, useState, Fragment, useContext } from "react";
import { StyleSheet, ScrollView, FlatList } from "react-native";

import { Text, View } from "../components/Themed";
import { getEntryList } from "../service/BotBService";
import Entry from "../model/Entry";
import EntryComponent from "../components/EntryComponent";
import RadioPlayerComponent from "../components/RadioPlayerComponent";
import ParticuleComponent from "../components/ParticuleComponent";

import { Audio } from "expo-av";
import { RadioContext } from "../App";
import { error } from "../service/NotifierService";

export default function RadioScreen() {
  const [entryList, setEntryList] = useState([] as Entry[]);
  const [currentEntryIndex, setCurrentEntryIndex] = useState(0);

  const { currentSound, botbServiceAnimation, play } = useContext(RadioContext);

  async function playSong(songPath: string) {
    try {
      const { sound } = await Audio.Sound.createAsync({ uri: songPath });

      await play(sound);
    } catch (e) {
      console.error(e);
      error("Cannot play current song..");
    }
  }

  useEffect(() => {
    getEntryList(0, 100)
      .then((res) => {
        const list: Entry[] = res.data;
        setEntryList(list);
      })
      .catch((e) => {
        console.error("Cannot fetch entry list");
        console.error(e);
        error("Cannot fetch remote radio..");
      });
  }, []);

  return (
    <View style={styles.screen}>
      <View style={styles.background}>
        <ParticuleComponent />
      </View>

      <View style={styles.container}>
        <FlatList
          style={styles.list}
          horizontal={false}
          data={entryList}
          renderItem={(current) => {
            return (
              <EntryComponent
                playing={current.index === currentEntryIndex}
                entry={current.item}
                onPress={async (event) => {
                  setCurrentEntryIndex(current.index);
                  await playSong(current.item.preview_url);
                }}
              />
            );
          }}
          keyExtractor={(item) => item.id}
        />
        {entryList.length > 0 && (
          <RadioPlayerComponent entry={entryList[currentEntryIndex]} />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  background: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    zIndex: 9,
    backgroundColor: "#00000000",
  },
  screen: {
    height: "100%",
    width: "100%",
    backgroundColor: "#232323"
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    zIndex: 99,
    backgroundColor: "#00000000",
  },
  list: {
    backgroundColor: "#00000000",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    backgroundColor: "#00000000",
  },
});
