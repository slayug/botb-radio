import React from "react";
import { StyleSheet, Platform, GestureResponderEvent } from "react-native";

import { View, Text } from "./Themed";
import Entry from "../model/Entry";

import ImageRatio from "./core/ImageRatio";
import { Config } from "../Config";
import Colors, { Tint } from "../constants/Colors";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function EntryComponent({
  entry,
  playing = false,
  onPress = () => {}
}: {
  entry: Entry;
  playing?: boolean;
  onPress?: (event: GestureResponderEvent) => void;
}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <ImageRatio
          width={65}
          src={`${Config.getMainSiteUrl()}${entry.botbr.avatar_url}`}
        />
        <View style={styles.text}>
          <Text style={[styles.title, playing && styles.playing]}>
            {entry.authors_display}
          </Text>
          <Text style={playing && styles.playing}>{entry.title}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const { r, g, b }= Tint.main;

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#00000000"
  },
  text: {
    marginLeft: 5,
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#00000000"
  },
  title: {},
  playing: {
    color: `rgb(${r}, ${g}, ${b})`,
  },
});
