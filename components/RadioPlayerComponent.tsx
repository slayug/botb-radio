import React, { useEffect, useContext, useState } from "react";
import { StyleSheet } from "react-native";

import { View, Text, useThemeColor } from "./Themed";
import Entry from "../model/Entry";

import { FontAwesome5 } from "@expo/vector-icons";
import ImageRatio from "./core/ImageRatio";
import { Config } from "../Config";
import IconButton from "./core/IconButton";
import { RadioContext } from "../App";
import { AVPlaybackStatus } from "expo-av";
import { toCssRgb, Tint } from "../constants/Colors";

export default function RadioPlayerComponent({ entry }: { entry: Entry }) {
  const [percentPlaying, setPercentPlaying] = useState(0);

  const { addSoundListener, playerService, currentSound, play, pause } = useContext(
    RadioContext
  );

  useEffect(() => {
    addSoundListener({
      newSong: () => setPercentPlaying(0),
      update: (status: AVPlaybackStatus) => {
        const percent = (status.positionMillis / status.durationMillis) * 100;
        if (percentPlaying !== percent) {
          setPercentPlaying(percent);
        }
      },
    });
  }, []);

  return (
    <View style={{ backgroundColor: "#36393F", width: "100%" }}>
      <View
        style={{
          width: percentPlaying + "%",
          height: 3,
          backgroundColor: toCssRgb(Tint.main),
        }}
      />
      <View style={styles.container}>
        <ImageRatio
          width={65}
          src={`${Config.getMainSiteUrl()}${entry.botbr.avatar_url}`}
        />
        <View style={styles.text}>
          <Text style={styles.title}>{entry.title}</Text>
          <Text style={styles.author}>{entry.authors_display}</Text>
        </View>
        <View style={styles.player}>
          {!playerService.isPlaying() ? (
            <IconButton
              icon={
                <FontAwesome5
                  name="play"
                  size={30}
                  color={useThemeColor({}, "icon")}
                />
              }
              onPress={play}
            />
          ) : (
            <IconButton
              icon={
                <FontAwesome5
                  name="pause"
                  size={30}
                  color={useThemeColor({}, "icon")}
                />
              }
              onPress={pause}
            />
          )}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",

    paddingRight: 10,

    // borderColor: "#FD009E",
    // borderTopWidth: 2,
  },
  text: {
    flexGrow: 1,
    paddingLeft: 5,
  },
  title: {
    color: toCssRgb(Tint.main),
  },
  author: {
    color: "#fefefe",
  },
  player: {},
});
