export interface DataMessageEvent { 
    type: DataMessageEventType;
    content: Object;
}

export enum DataMessageEventType {
    CANVAS_READY,
    
}