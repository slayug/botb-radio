import { DataMessageEventType } from "./DataMessageEvent";

const appContent = `

    class BotBService {
        constructor() {
            this.currentColor = {
                r: 255,
                g: 119,
                b: 255
            }
            this.playing = false;
            this.prevPlaying = false;
        }

        setCurrentColor(color) {
            this.currentColor = color;
        }

        isPlaying() {
            return this.playing;
        }

        play() {
            if (!this.playing) {
                this.prevPlaying = false;
                this.playing = true;
                drawParticule();
            }
        }

        stop() {
            if (this.playing) {
                this.prevPlaying = true;
            }
            this.playing = false;
        }

        isPrevPlaying() {
            return this.prevPlaying;
        }

    }

    function onMessage(message) {
        const task = JSON.parse(message.data);
        const toast = document.getElementById("toast");

        const { obj, method, args } = task;
        //log('obj' + obj)
        //log('method ' + method)
        //log('args ' + JSON.stringify(args))
        targets[obj][method](args);
    }

    // iOS
    window.addEventListener('message', onMessage);
    // Android
    document.addEventListener('message', onMessage);

    const canvas = document.getElementById('canvas');
    const context = canvas.getContext('2d');
    const botbService = new BotBService();

    const targets = [];
    targets['context2D'] = context;
    targets['document'] = document;
    targets['botbService'] = botbService;


    // from https://github.com/YanYuanFE/react-native-signature-canvas/blob/master/h5/js/app.js
    // Adjust canvas coordinate space taking into account pixel ratio,
    // to make it look crisp on mobile devices.
    // This also causes canvas to be cleared.
    function resizeCanvas() {
        // When zoomed out to less than 100%, for some very strange reason,
        // some browsers report devicePixelRatio as less than 1
        // and only part of the canvas is cleared then.
        var context = canvas.getContext("2d");
        var ratio =  Math.max(window.devicePixelRatio || 1, 1);

        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        context.scale(ratio, ratio);
    }
    
    window.onresize = resizeCanvas;
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    // resizeCanvas();

    const webView = window.ReactNativeWebView;
     webView.postMessage(JSON.stringify({
        type: ${DataMessageEventType.CANVAS_READY},
        content: "toast"
    }));

    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    const particules = [...Array(150).keys()].map((index) => {
        const size = getRandomArbitrary(1, 8);
        return {
          x: getRandomArbitrary(0, canvas.width),
          y: getRandomArbitrary(canvas.height, canvas.height * 2),
          size: size,
          opacity: parseInt(size * 10) / 100,
          speed: getRandomArbitrary(2, 10),
          r: 255,
          g: 119,
          b: 255
        };
    });

    function resetParticule() {
        particules.forEach((particule) => {
            particule.opacity = getRandomArbitrary(2, 10);
            particule.y = getRandomArbitrary(canvas.height, canvas.height * 2);
        });
    }

    let opacityRatio = 1;
    let clearStep = 0;
    let speedOrientation = 1;
    function drawParticule() {
        if (botbService.isPrevPlaying() && !botbService.isPlaying()) {
            opacityRatio = 95/105;
            clearStep += 1;
            speedOrientation = -1;
            if (clearStep >= 400) {
                clearStep = 0;
                return;
            }
        } else if (!botbService.isPrevPlaying() && botbService.isPlaying() && opacityRatio !== 1) {
            resetParticule();
            speedOrientation = 1;
            opacityRatio = 1;
        }

        requestAnimationFrame(drawParticule);
        
        context.clearRect(0, 0, canvas.width, canvas.height);
        particules.forEach((particule) => {
            const { x, y, speed, opacity } = particule;
            const { r, g, b } = botbService.currentColor;

            context.fillStyle = "rgba(" + r +
                                      "," + g +
                                      "," + b +
                                      "," + opacity + ")";

            particule.y -= 0.1 * speed * speedOrientation;
            particule.opacity = particule.opacity * opacityRatio;

            y < 0 && (particule.y = canvas.height);
            y < 0 && (particule.speed = getRandomArbitrary(2, 10));

            context.fillRect(particule.x, particule.y, particule.size, particule.size);
        });
    }
`;

export default appContent;
