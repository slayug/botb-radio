import React, { useContext } from "react";
import { WebView, WebViewMessageEvent } from "react-native-webview";
import { StyleProp } from "react-native";
import { useEffect, useState, useRef } from "react";

import htmlSource from "./html";
import { DataMessageEvent, DataMessageEventType } from "./DataMessageEvent";
import { context2DMethods } from "./Canvas";
import { Tint } from "../../constants/Colors";
import { RadioContext } from "../../App";

type Task = {
  obj: "document" | "canvas" | "context2D" | "botbService";
  /**
   * Allow sub object calls
   */
  subObj?: string[];
  method: string;
  args: string[];
};

const BotBServiceMethods = [
  'setCurrentColor',
  'play',
  'stop'
]

export default function CanvasComponent({
  on2DContextReady,
}: {
  on2DContextReady: (canvas: CanvasRenderingContext2D) => void;
}) {
  const webViewRef = useRef();
  const [context2D, setContext2D] = useState({} as CanvasRenderingContext2D);
  const [botbService, setBotbService] = useState({});

  const { setBotbServiceAnimation } = useContext(RadioContext);

  useEffect(() => {
    //setup canvas mock
    const newContext2D = new Object();
    context2DMethods.forEach((method) => {
      newContext2D[method] = (args) => {
        sendTask({
          obj: "context2D",
          method,
          args
        })
      };
    });
    setContext2D(newContext2D);

    const newBotbService = new Object();
    BotBServiceMethods.forEach((method) => {
      newBotbService[method] = (args) => {
        sendTask({
          obj: "botbService",
          method,
          args
        })
      }
    })
    setBotbService(newBotbService);
    setBotbServiceAnimation(newBotbService);
  }, []);

  function sendTask(task: Task) {
    if (webViewRef) {
      webViewRef.current.postMessage(JSON.stringify(task));
    }
  }

  function loadEnd() {
    on2DContextReady(context2D)
    botbService.setCurrentColor(Tint.main)
  }

  function onMessage(event: WebViewMessageEvent) {
    try {
      const dataMessageEvent: DataMessageEvent = JSON.parse(
        event.nativeEvent.data
      );

      switch (dataMessageEvent.type) {
        case DataMessageEventType.CANVAS_READY:
          console.log("canvas ready ", dataMessageEvent.content);
          break;
      }
    } catch (e) {
      console.error("Cannot handle current event", event);
    }
  }

  return (
    <WebView
      ref={webViewRef}
      originWhitelist={["*"]}
      source={{ html: htmlSource }}
      javaScriptEnabled={true}
      androidHardwareAccelerationDisabled={false}
      onMessage={onMessage}
      onLoadEnd={() => loadEnd()}
    />
  );
}
