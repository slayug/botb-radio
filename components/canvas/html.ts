import script from './app';

const content =
    `<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>BotB</title>
  <meta name="description" content="BotB Radio">

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <style>
    body {
      padding: 0;
      margin: 0;
      height: 100%;
      width: 100%;
      background-color: #232323;
      color: #232323;
    }
    canvas {
      height: "100%";
      width: "100%";
      padding: 0;
      margin: 0;
    }
    #logs {
      position: absolute;
      top: 0;
      right: 0;
      background-color: #ffffff60;
      color: "#232323;
      padding: 5px;
      overflow-y: auto;
      height: 700px;
    }
  </style>
</head>
<body>
  <div id="logs"></div>
  <canvas id="canvas"></canvas>
  <script>

  let logs = '';

  const logsElement = document.getElementById("logs");
  function log(message) {
    logs = logs + message + '<br />';
    logsElement.innerHTML = logs;
  }
  setInterval(() => {
    logsElement.scrollTop = logsElement.scrollHeight;
  }, 800);

  window.addEventListener("load", () => {
    try {
    ${script}
    } catch (e) {
      // catching any error from app
      document.body.style.color = "#e74c3c";
      document.body.innerHTML = e;

    }
  });

  </script>
</body>
</html>`;

export default content;
