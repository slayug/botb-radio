import React, { useEffect, useState, useRef } from "react";
import { View } from "./Themed";
import { Animated } from "react-native";

import { StyleSheet } from "react-native";
import { Tint } from "../constants/Colors";

import { Dimensions } from "react-native";
import CanvasComponent from "./canvas/CanvasComponent";


type Particule = {
  x: number;
  y: number;
  size: number;
  opacity: number;
  speed: number;
  color: string;
};

export default function ParticuleComponent() {

  function on2DContextReady(context: CanvasRenderingContext2D) {
    // context.fillRect(2, 2, 10, 10);
  }

  return <CanvasComponent on2DContextReady={on2DContextReady} />
}

const styles = StyleSheet.create({
  container: {
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
});
