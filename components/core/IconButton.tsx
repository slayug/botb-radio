import React from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { View } from "react-native";

export default function IconButton({ icon, onPress }: { icon: JSX.Element, onPress?: Function}) {
  return (
    <TouchableOpacity onPress={() => onPress()}>
      <View>{icon}</View>
    </TouchableOpacity>
  );
}
