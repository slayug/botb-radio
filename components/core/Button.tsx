import {
  Button as NativeButton,
  ButtonProps,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  PixelRatio,
} from "react-native";
import React from "react";

const Button = (props: {
  title: string;
  disable?: boolean;
  onPress?: Function;
  style?: Object;
}) => {
  let currentStyle = [styles.button];
  if (props.style) {
    currentStyle = StyleSheet.flatten([styles.button, props.style]);
  }
  if (props.disable) {
    currentStyle = StyleSheet.flatten([currentStyle, styles.disable]);
  }

  return (
    <View style={[currentStyle]}>
      <TouchableOpacity
        style={styles.touchable}
        onPress={() => {
          if (!props.disable && props.onPress) {
            props.onPress();
          }
        }}
      >
        <Text style={styles.text}>{props.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#a2a2a2",
    borderRadius: 4,
    height: 35,
    padding: 5,
  },
  touchable: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
  },
  disable: {
    backgroundColor: "#dfe6e9",
  },
  text: {
    fontSize: 8 * PixelRatio.get(),
    color: "#fff",
    textAlign: "center",
  },
});

export default Button;
