import React, { useEffect, useState } from "react";
import { Image, TouchableOpacity } from "react-native";

export default function ImageRatio({
  src,
  width,
  height,
  onClick,
}: {
  src: string;
  width?: number | string;
  height?: number | string;
  onClick?: undefined | Function;
}) {
  const [ratio, setRatio] = useState(1);

  useEffect(() => {
    Image.getSize(src, (w, h) => {
      if (!isNaN(w / h)) {
        setRatio(w / h);
      }
    });
  }, []);

  const style = {
    aspectRatio: ratio,
  };
  if (width && height) {
    //ignore ratio
    style.width = width;
    style.height = height;
    style.aspectRatio = 1;
    style.resizeMode = "cover";
  } else if (width) {
    style.width = width;
    if (ratio === 1) {
      style.height = width;
    }
  } else if (height) {
    style.height = height;
    if (ratio === 1) {
      style.width = height;
    }
  } else {
    style.width = 60;
  }

  function renderImage() {
    return (
      <Image
        source={{
          uri: src,
        }}
        style={style}
      />
    );
  }

  if (onClick) {
    return <TouchableOpacity onPress={onClick}>{renderImage()}</TouchableOpacity>;
  } else {
    return renderImage();
  }
}
