const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';

export type Color = {
  r: number;
  g: number;
  b: number;
}

export const Tint = {
  main: {
    r: 253,
    g: 0,
    b: 158,
  }
}

export function toCssRgb(color: Color) {
  const { r, g, b } = color;

  return `rgb(${r}, ${g}, ${b})`;
}

export default {
  light: {
    text: 'white',
    background: '#202225',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    icon: toCssRgb(Tint.main)
  },
  dark: {
    text: '#fc7100',
    background: '#202225',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    icon: "white"
  },
};
