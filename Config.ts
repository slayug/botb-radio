const config = {
    mainSiteUrl: "https://battleofthebits.org",
    botbApiUrl: "/api/v1" 
}

export class Config {

    static getBotBApiUrl() {
        return config.mainSiteUrl + config.botbApiUrl;
    }

    static getMainSiteUrl() {
        return config.mainSiteUrl;
    }

}